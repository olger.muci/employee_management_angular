import { EmployeeService } from '../employee.service';
import { Employee } from '../employee';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Role } from '../role';
import { Observable } from 'rxjs';
import { RoleService } from '../role.service';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {

  employee: Employee = new Employee();
  roles: any=[];
  submitted = false;

  constructor(private roleService: RoleService,private employeeService:EmployeeService,
    private router: Router) { }

  ngOnInit() {
    this.roles = this.roleService.getRoleList().subscribe(response =>{
      this.roles=response
    });
  }

  newEmployee(): void {
    this.submitted = false;
    this.employee = new Employee();
  }

  save() {
    this.employeeService.getEmployeeByEmail(this.employee.emailId)
    .subscribe(data => {
      console.log(data)
    if(data==null){
this.employeeService
    .createEmployee(this.employee).subscribe(data => {
      console.log(data)
      this.employee = new Employee();
      this.gotoList();
    }, 
    error => console.log(error));
    }else{
      alert("User already exists")
      this.resetform()
    }
    }
      , error => console.log(error));
    
  }
  resetform(){
this.employee.emailId="";
        this.employee.password="";
        this.employee.firstName="";
        this.employee.lastName=""
        this.gotoCreate()
}
  gotoCreate() {
    this.submitted=false
    this.router.navigate(['/create']);
  }

  onSubmit() {
    this.submitted = true;
    this.save();    
  }

  gotoList() {
    this.router.navigate(['/employee-list/1']);
  }
}
