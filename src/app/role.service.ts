import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  private apiLink
   = 'http://localhost:8080/employee-management/api/v1/roles';

  constructor(private http: HttpClient) { }

  getRoles(id: number): Observable<any> {
    return this.http.get(`${this.apiLink
    }/${id}`);
  }

  createRole(employee: Object): Observable<Object> {
    return this.http.post(`${this.apiLink
    }`, employee);
  }

  updateRole(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.apiLink
    }/${id}`, value);
  }

  deleteRole(id: number): Observable<any> {
    return this.http.delete(`${this.apiLink
    }/${id}`, { responseType: 'text' });
  }

  getRoleList(): Observable<any> {
    return this.http.get(`${this.apiLink
    }`);
  }
}
