import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from '../employee.service';
import { RoleService } from '../role.service';
import { Employee } from '../employee';
import { BehaviorSubject } from 'rxjs';
import { DbEmployee } from '../dbemployee';
import { Role } from '../role';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email:string=""

  submitted = false;
  dbemployee:Employee=new Employee;
  user={
    email:'',
    password:''
  }
constructor(private route: ActivatedRoute,private roleService: RoleService,private employeeService:EmployeeService,
    private router: Router) { 
  }
  ngOnInit(): void {
    
  }

onSubmit() {
    this.submitted = true;
    console.log("test")
    // Validate password for empty or whitespace
    if (this.user.password === '' || this.user.password.trim().length === 0) {
      alert('Password field should not be empty');
      this.resetform()
      return;
    }
    // Proceed with the login process
    this.login(this.user.email);  
}
 login(email:string) {
 this.employeeService.getEmployeeByEmail(email)
    .subscribe(data => {
      console.log(data)
      this.dbemployee=data
      if(data==null){
        alert("User does not exist");
        this.resetform()
      }else{
      if(this.dbemployee.password=== this.user.password && this.user.password.length > 0){
        this.gotoList(this.dbemployee.role.id)
      }else{
        alert("Wrong Password")
        this.resetform()
      }
    }}, error => console.log(error));
}
resetform(){
this.user.email="";
        this.user.password="";
        this.gotoLogin()
}

  gotoList(role:number) {
    this.router.navigate(['/employee-list',role]);
  }
  gotoLogin() {
  this.router.navigate(['/login']);
}
}


