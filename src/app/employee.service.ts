import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private apiLink
   = 'http://localhost:8080/employee-management/api/v1/employees';

  constructor(private http: HttpClient) { }

  getEmployee(id: number): Observable<any> {
    return this.http.get(`${this.apiLink
    }/${id}`);
  }
  getEmployeeByEmail(email:string): Observable<any> {
    return this.http.get(`${this.apiLink
    }/${email}/get`);
  }

  createEmployee(employee: Object): Observable<Object> {
    return this.http.post(`${this.apiLink
    }`, employee);
  }

  updateEmployee(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.apiLink
    }/${id}`, value);
  }

  deleteEmployee(id: number): Observable<any> {
    return this.http.delete(`${this.apiLink
    }/${id}`, { responseType: 'text' });
  }

  getEmployeesList(): Observable<any> {
    return this.http.get(`${this.apiLink
    }`);
  }
}
