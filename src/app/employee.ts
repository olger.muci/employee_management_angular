import { Role } from "./role";

export class Employee {
  id :number=0;
  firstName: string="";
  lastName: string="";
  emailId: string="";
  password: string="";
  role: Role={
    id: 0,
    role: ""
  };
  active: boolean=false;
}
