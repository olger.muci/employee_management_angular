import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
gotoSignup() {
this.router.navigate(['/create']);
}
role: any|string;
  constructor(private router: Router){
  }
submited:boolean= false;
 gotoLogin() {
  this.submited=true;
  this.router.navigate(['/login']);
}

}
